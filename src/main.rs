mod db;

use serenity::client::EventHandler;
use serenity::model::channel::Message;
use serenity::{async_trait, http};
use serenity::{framework::standard::macros::group, model::id::UserId};

use std::{env, thread, time::Duration, u64};

use egg_mode::tweet::DraftTweet;

#[group]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[derive(Clone, Debug)]
struct BotConfig {
    emoji: String,
    target_user: UserId,
    twitter_token: egg_mode::KeyPair,
    twitter_app_token: egg_mode::KeyPair,
}

#[tokio::main]
async fn main() -> ! {
    dotenv::dotenv().expect("Failed to load .env file");

    // Start the db
    db::init().unwrap();

    // Login with a bot token from the environment
    let t = env::var("DISCORD_TOKEN").expect("token");
    let httpc = http::client::Http::new_with_token(&t);

    let target_user = env::var("THE_CODY").expect("Cody user_id");
    let target_emoji = env::var("EMOJI").expect("Reaction emoji");
    let twitter_api = env::var("TWITTER_KEY").expect("Twitter API key");

    let twitter_secret = env::var("TWITTER_SECRET").expect("Twitter secret key");
    let twitter_token = egg_mode::KeyPair::new(twitter_api, twitter_secret);

    let twitter_app = env::var("TWITTER_APP_TOKEN").expect("Twitter APP access token");
    let twitter_app_secret = env::var("TWITTER_APP_SECRET").expect("Twitter APP secret token");
    let twitter_app_token = egg_mode::KeyPair::new(twitter_app, twitter_app_secret);

    let config = BotConfig {
        emoji: target_emoji,
        target_user: UserId(target_user.as_str().parse::<u64>().unwrap()),
        twitter_token: twitter_token,
        twitter_app_token: twitter_app_token,
    };

    loop {
        println!("Polling...");
        let res = http::client::Http::get_messages(&httpc, 285493068222955522, "?limit=100");

        let messages = res.await;
        match messages {
            Ok(msgs) => {
                let the_cody = config.target_user;
                let emoji = config.emoji.to_string();

                for m in msgs {
                    // TODO: Add attachment support later
                    if m.content.to_string() == "" {
                        continue;
                    }

                    if !is_cody_message(m.author.id, the_cody) {
                        continue;
                    }

                    for reaction in &m.reactions {
                        if reaction.reaction_type.to_string() == emoji {
                            println!("Found one! {:?}", &m);
                            if !db::does_message_exist(*m.id.as_u64()).unwrap() {
                                db::insert_message(*m.id.as_u64(), m.content.to_string());

                                //
                                let token = egg_mode::Token::Access {
                                    consumer: config.clone().twitter_token,
                                    access: config.clone().twitter_app_token,
                                };

                                let res = DraftTweet::new(m.content.to_string()).send(&token).await;
                                match res {
                                    Ok(v) => println!("Success! {:?}", v),
                                    Err(e) => println!("Error! {:?}", e),
                                }
                            //
                            } else {
                                println!("Ignoring {} - {}", *m.id.as_u64(), m.content.to_string())
                            }
                        }
                    }
                }
            }
            Err(e) => println!("Couldn't get messages: {:?}", e),
        }

        thread::sleep(Duration::from_secs(15));
    }
}

fn is_cody_message(user_id: UserId, target_user_id: UserId) -> bool {
    if user_id == target_user_id {
        return true;
    }

    false
}

// async fn spawn_tweet(message: String, config: BotConfig) {
//     let token = egg_mode::auth::bearer_token(&config.twitter_token)
//         .await
//         .unwrap();
//     println!("Tweetered: {:?}", message);
//     DraftTweet::new(message).send(&token).await.unwrap();
//     ()
// }

// async fn tweet(message: String, config: &BotConfig) {
//     let new_c = config.clone();

//     tokio::task::spawn(async move { spawn_tweet(message, new_c) })
//         .await
//         .expect("PANIC AT THE TWEET")
//         .await;
//     ()
// }
