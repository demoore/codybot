use rusqlite::{params, Connection, Result};

pub fn init() -> Result<()> {
    let conn = Connection::open("messages.db")?;
    conn.execute(
        "create table if not exists messages (
id integer primary key,
message text not null
)",
        [],
    )?;

    Ok(())
}
pub fn does_message_exist(id: u64) -> Result<bool, &'static str> {
    let conn = Connection::open("messages.db").unwrap();

    let mut stmt = conn
        .prepare("select id from messages where id=(?1)")
        .unwrap();

    let result = stmt
        .query_map(
            params![&id],
            |_| -> std::result::Result<bool, rusqlite::Error> { Ok(true) },
        )
        .unwrap();

    if result.count() >= 1 {
        return Ok(true);
    } else {
        return Ok(false);
    };
}

pub fn insert_message(id: u64, message: String) {
    let conn = Connection::open("messages.db").unwrap();
    conn.execute(
        "insert into messages (id, message) values (?1, ?2)",
        params![id, message],
    )
    .unwrap();
    ()
}
